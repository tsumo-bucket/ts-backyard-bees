@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminProfile') }}">Profile</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>Change Password</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>Change Password</h1>
    <div class="header-actions">
        <a href="{{ route('adminProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
    <div class="row">
      <div class="col-sm-12">
        <div class="caboodle-card">
          <div class="caboodle-card-header">
            <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
          </div>
          <div class="caboodle-card-body">
            {!! Form::model($user, ['route'=>['adminProfilePasswordUpdate'], 'method' => 'patch', 'class'=>'form form-parsley']) !!}
            <div class="caboodle-form-group">
              <label for="current">Current</label>
              {!! Form::password('current', ['class'=>'form-control', 'id'=>'current', 'placeholder'=>'', 'required']) !!}
            </div>
            <div class="caboodle-form-group">
              <label for="new">New</label>
              {!! Form::password('new', ['class'=>'form-control', 'id'=>'new', 'placeholder'=>'', 'required', 'data-parsley-minlength'=>'6']) !!}
            </div>
            <div class="caboodle-form-group">
              <label for="new_confirmation">Re-type new</label>
              {!! Form::password('new_confirmation', ['class'=>'form-control', 'id'=>'new_confirmation', 'placeholder'=>'', 'required', 'data-parsley-equalto'=>'#new', 'data-parsley-minlength'=>'6']) !!}
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection