@foreach($data->controls as $key => $control)
	<!-- PRODUCTS SELECTOR -->
	@if(@$control->type == 'products')
		<div
			class="caboodle-form-group control-group product-selector margin-top"
			data-id="product-selector-{{$control->id}}"
			control={{ base64_encode(json_encode($control)) }}
		>
		</div>
	@endif

	<!-- DATE -->
	@if($control->type == 'date')
	<div class="caboodle-form-group control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable == 1)
		<input type="text" 
				class="date-picker"
				name="{{ $control->name}}"
				@if ($control->required == 1) required @endif 
				maxlength="255" value="{{ Carbon::parse(@$control->value)->format('M d, Y') }}">
		@else
			{{Carbon::parse($control->value)->format('F d, Y')}}
		@endif
	</div>
	@endif

	<!-- TIME -->
	@if($control->type == 'time')
	<div class="caboodle-form-group control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable == 1)
		<input type="text" 
				class="time-picker"
				name="{{ $control->name}}" 
				@if ($control->required == 1) required @endif 
				maxlength="255" value="{{ @$control->value }}">
		@else
			{{Carbon::parse('1999-01-01 ' . $control->value)->format('h:iA')}}
		@endif 
	</div>
	@endif

	<!-- DATE TIME -->
	@if($control->type == 'date_time')
	<div class="caboodle-form-group  control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable == 1)
		<input type="text" 
				class="datetime-picker"
				name="{{ $control->name}}" 
				@if ($control->required == 1) required @endif 
				maxlength="255" value="{{ @$control->value }}">
		@else
			{{Carbon::parse($control->value)->format('F d, Y h:iA')}}
		@endif 
	</div>
	@endif

	<!-- TEXT -->
	@if($control->type == 'text')
	<div class="caboodle-form-group  control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable == 1)
		<input type="text" 
				name="{{ $control->name }}" 
				init-translatable=""
				@if ($control->required == 1) required @endif 
				maxlength="255" value="{{ @$control->value }}">
		@else
			{{$control->value}}
		@endif 
	</div>
	@endif
	
	<!-- TEXTAREA -->
	@if($control->type == 'textarea')
	<div class="caboodle-form-group  control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable != 1)
			<div class="word-break">{!! $control->value !!}</div>  
		@else
			<textarea name="{{ $control->name }}" 
				class="form-control redactor" 
				data-redactor-upload="{{ route('adminAssetsRedactor') }}"
				init-translatable=""
				@if ($control->required == 1) required @endif>
				{{ $control->value }}
			</textarea>
		@endif
	</div>
	@endif

	<!-- NUMBER -->
	@if($control->type == 'number')
	<div class="caboodle-form-group  control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		@if ($data->editable == 1)
		<input type="number" 
				name="{{ $control->name }}" 
				init-translatable=""
				@if ($control->required == 1) required @endif 
				maxlength="255" value="{{ @$control->value }}">
		@else
		{{$control->value}}
		@endif 
		
	</div>
	@endif

	<!-- CHECKBOX -->
	@if($control->type == 'checkbox')
	<div class="mdc-form-field label-left">
	@if($data->editable == 1)
		<div class="mdc-checkbox">
			<input type="checkbox" id="{{ $control->name }}" name="{{ $control->name }}" class="mdc-checkbox__native-control" {{ @$control->value == "on" ? "checked" : "" }} />
			<div class="mdc-checkbox__background">
				<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
					<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
				</svg>
				<div class="mdc-checkbox__mixedmark"></div>
			</div>
		</div>
	@endif
		<label for="{{ $control->name }}">{{ $control->label }}</label>
	@if($data->editable == 0)
		@if($control->value == 'on')
		<i class="fas fa-check" style="margin-left:20px;font-size:20px"></i>
		@else
		<i class="fas fa-times" style="margin-left:20px;font-size:20px"></i>
		@endif
	@endif
	</div>
	<br><br>
	@endif

	<!-- ASSET -->
	@if($control->type == 'asset')
		
		@if($data->editable == 1)
		<div class="caboodle-form-group  control-group sumo-asset-select"   control={{ base64_encode(json_encode($control)) }}>
			<label for="{{ $control->name }}" class="caboodle-flex caboodle-flex-space-between caboodle-flex-align-center">{{ $control->label }}</label>
			{!! Form::hidden($control->name, $control->value, ['class'=>'sumo-asset', (($control->required == 1) ? 'required' : ''), 'data-id'=>@$control->id, 'data-thumbnail'=>'image_thumbnail', 'init-translatable']) !!}
		</div>
		@else
		
		<div class="caboodle-form-group  control-group"  control={{ base64_encode(json_encode($control)) }}>
			<label for="{{ $control->name }}" class="caboodle-flex caboodle-flex-space-between caboodle-flex-align-center">{{ $control->label }}</label>
			<div>
				<img src="@{{AssetPath::asset(@$control->asset->medium_thumbnail)}}"  style="object-fit:contain; width: 250px;height: 250px; background: #fafbfc; border: 1px solid rgba(195, 207, 216, 0.3);" />
			</div>
		</div>
		
		 @endif
		
	@endif

	<!-- SELECT -->
	@if ($control->type == 'select')
	<?php
		$options = [];
		try {
			$options = json_decode($control->options_json);
		} catch(Exception $e) {}
	?>
	<div class="caboodle-form-group control-group"  control={{ base64_encode(json_encode($control)) }}>
		<label for="{{ $control->name }}">{{ $control->label }}</label>
		
		@if($data->editable == 1)
		<select name="{{ $control->name }}" class="sumo-select" id="{{ $control->name }}" @if ($control->required == 1) required @endif>
			@foreach($options as $option)
				<option value="{{ $option->value }}" @if ($control->value == $option->value) selected @endif>{{ $option->label }}</option>
			@endforeach
		</select>
		@else
		@foreach($options as $option)
			{{ $option->label }}
		@endforeach
		@endif
	</div>
	@endif
@endforeach
<?php
// @section('added-scripts')
// @parent
// 	<script>
// 		$(document).ready(function() {
			
// 			$.each($(".product-selector"), function() {
// 				var control = atob($(this).attr("control"));
// 				var id = $(this).data("id");

// 				if (control) {
// 					control = JSON.parse(control);
// 				}
				
// 				$(this).append(`
// 					<div id="container${control.id}">
// 						<div class="flex align-center caboodle-form-control-connected">
// 							<h4 class="no-margin flex-1">
// 								${control.label}
// 							</h4>
// 							<button
// 								type="button"
// 								class="caboodle-btn caboodle-btn-x-small caboodle-btn-primary mdc-button mdc-button--unelevated mdc-ripple-upgraded ${id}"
// 							>
// 								<i class="far fa-plus-circle"></i> Add
// 							</button>
// 						</div>
// 						<div id="${id}_selected">
// 							<div class="empty-message margin-top">
// 								Please select a product.
// 							</div>
// 						</div>
// 					</div>
// 				`);

// 				initProductSelector($("." + id), ("#" + id + "_selected"), control.id, control.products ? control.products : false);
// 			});

// 			function initProductSelector(element, container, dataID, products) {
// 				var productSelector = element.uniSelector({
// 					dataCategory: 'Products', //-- This will serve as the data name for the uni-selector
// 					title: 'Select Products', //-- A custom title header for the uni-selector
// 					confirmButtonText: 'Select', //-- A custom text for the confirm button of the uni-selector
// 					disableSelected: true,
// 					filters: [
// 						{
// 							"name": "title",
// 							"type": "text",
// 							"placeholder": "Prodcut name",
// 							"width": "50%",
// 							"icon": "far fa-search",
// 							"query": {
// 								"method": "where",
// 								"logic": "LIKE",
// 								"wildcard": "end"
// 							}
// 						},
// 						{
// 							"name": "category",
// 							"type": "select",
// 							"options": {
// 								"text": "title",
// 								"value": "id",
// 								"objects": {!! General::getProductCategoryList() !!}
// 							},
// 							"placeholder": "Category",
// 							"width": "25%",
// 							"icon": "far fa-list",
// 							"query": {
// 								"method": "whereHas",
// 								"logic": "=",
// 								"relationship": {
// 									"model": "ProductCategory",
// 									"method": "where",
// 									"field": "id",
// 									"logic": "="
// 								}
// 							}
// 						},
// 						{
// 							"name": "brand",
// 							"type": "select",
// 							"options": {
// 								"text": "title",
// 								"value": "id",
// 								"objects": {!! General::getProductBrandsList() !!}
// 							},
// 							"placeholder": "Brand",
// 							"width": "25%",
// 							"icon": "far fa-list",
// 							"query": {
// 								"method": "whereHas",
// 								"logic": "=",
// 								"relationship": {
// 									"model": "ProductVendor",
// 									"method": "where",
// 									"field": "id",
// 									"logic": "="
// 								}
// 							}
// 						}
// 					],
// 					apiURL: "{{route('adminCaboodleUniSelectorProducts')}}", //-- A route for the API endpoint where the data will be fetched,
// 					selectedDataContainer: container, //-- An ID or a class of an element(s) that will serve as a container of the selected items
// 					emptyMessage: "This add-on is currently not connected to any product." //-- An empty message for the selectedDataContainer
// 				});
				
// 				productSelector.onConfirm(function (selected, addedItems) {
// 					$("#container" + dataID).find("[name='products[" + dataID + "][]']").remove();
// 					_.each(selected, function(item) {
// 						// !NOTE -- item.key is derived from the id value
// 						$("#container" + dataID).append(`<input type="hidden" name="products[${dataID}][]" value="${item.key}" />`);
// 					});
// 				});

// 				var onConfirm = function(selected) {
// 					$("#container" + dataID).find("[name='products[" + dataID + "][]']").remove();
// 					_.each(selected, function(item) {
// 						// !NOTE -- item.key is derived from the id value
// 						$("#container" + dataID).append(`<input type="hidden" name="products[${dataID}][]" value="${item.key}" />`);
// 					});
// 				}

// 				if (products) {
// 					productSelector.initializeData(products, onConfirm);
// 				}
				
// 			}
// 		});
// 	</script>
// @stop
?>