<div class="modal fade" id="assets-modal" tabindex="-1" role="dialog" aria-labelledby="assetsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Select File</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row loader-asset">
            <div class="col-sm-12">
              <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                </svg>
              </div>
            </div>
          </div>
          <div class="row row-asset hide">
            <div class="col-sm-8">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="nav-item"><a class="nav-link active" id="file-upload-tab" href="#file-upload" aria-controls="file-upload" role="tab" data-toggle="tab">Upload</a></li>
                <li role="presentation" class="nav-item"><a class="nav-link" id="file-library-tab" href="#file-library" aria-controls="file-library" role="tab" data-toggle="tab">Library</a></li>
              </ul>
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="file-upload" role="tabpanel" aria-labelledby="file-upload-tab">
                  {!! Form::open(['route'=>'adminAssetsUpload', 'id'=>'file-upload-form', 'files'=>true, 'class'=>'hide']) !!}
                  <input type="file" class="hide input-file" name="photos[]" multiple>
                  {!! Form::close() !!}
                  <div class="caboodle-form-group">
                    <label>Tags</label>
                    <select class="select2 sumo-asset-tagger"
                    multiple
                    data-url="{{route('adminGetAssetTags')}}"
                    ></select>
                  </div>
                  <div id="file-dropzone" class="dropzone display-table">
                    <div class="display-cell text-center">Drop files here to upload</div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="file-library" role="tabpanel" aria-labelledby="file-library-tab">
                  <div class="caboodle-form-group label-left search-container">
                    <label>Search:</label>
                    <input type="text" class="form-control" name="searchInput" placeholder="Name">
                  </div>
                  <div class="caboodle-form-group label-left sort-container">
                    <label>Sort: </label>
                    <select class="form-control" name="sortBy">
                      <option value="latest">Latest to oldest</option>
                      <option value="oldest">Oldest to latest</option>
                      <option value="az">A to Z</option>
                      <option value="za">Z to A</option>
                    </select>
                  </div>
                  <div class="caboodle-form-group label-left filter-container">
                    <label>Filter:</label>
                    <select class="select-filter-type form-control" name="filterBy">
                      <option value="file_type">File Type</option>
                      <option value="created_at">Uploaded Date</option>
                    </select>
                    <select class="select-file-type form-control" name="filterByFileType">
                      <option value="all">All</option>
                      <option value="image">Image</option>
                      <option value="audio">Audio</option>
                      <option value="video">Video</option>
                      <option value="pdf">PDF</option>
                      <option value="document">Document</option>
                      <option value="spreadsheet">Spreadsheet</option>
                      <option value="presentation">Presentation</option>
                      <option value="other">Other</option>
                    </select>
                    <div class="date-range" style="display: none;">
                      <button class="caboodle-btn mdc-button caboodle-btn-small date-filter btn-icon-both" id="upload-daterange">
                        <i class="far fa-calendar-alt"></i>
                        <span>Last 30 Days</span>
                        <i class="fas fa-caret-down"></i>
                      </button>
                      <input type="hidden" name="filterByDateFrom" />
                      <input type="hidden" name="filterByDateTo" />
                      <button id="filterByDateBtn" class="caboodle-btn mdc-button caboodle-btn-small caboodle-btn-secondary">Filter</button>
                    </div>
                  </div>
                  <div class="caboodle-form-group">
                    <label>Tags</label>
                    <div class="sumo-tag-holder"></div>
                  </div>

                  <div class="files" data-url="{{route('adminAssetsAll')}}">
                    <div class="file clone hide" data-id="">
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="load-more text-center">
                    <button type="button" class="caboodle-btn mdc-button caboodle-btn-secondary btn-more">Load More</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="loader loader-detail hide">
                <svg class="circular" viewBox="25 25 50 50">
                  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                </svg>
              </div>
              <div class="asset-details hide" data-url="{{route('adminAssetsGet')}}">
                <h4>Details</h4>
                {!! Form::open(['route'=>'adminAssetsUpdate', 'id'=>'asset-detail-form']) !!}
                  <small></small>
                  <div class="photo">
                    {{ HTML::image('upload/images/bamboo.jpg') }}
                  </div>
                  <div class="delete">
                    <span class="download-btn">
                      <a class="caboodle-link" href="{{route('adminAssetsDownload')}}">Download</a>
                    </span>
                    <span class="delete-btn">
                      <a class="caboodle-link" href="#">Delete</a>
                    </span>
                    <span class="confirm-btn hide">
                      Are you sure?
                      <a class="confirm-delete-btn" href="{{route('adminAssetsDestroy')}}">Yes</a>
                      &middot;
                      <a href="#">No</a>
                      </br><span style="margin-right:3px;" class="fa fa-exclamation-triangle"></span>If you delete this file it will affect web content connected to this.
                    </span>
                  </div>
                  <div class="caboodle-form-group">
                    <label for="name">Name</label>
                    {!! Form::hidden('id', null) !!}
                    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name']) !!}
                  </div>
                  <div class="caboodle-form-group">
                    <label for="name">Caption</label>
                    {!! Form::text('caption', null, ['class'=>'form-control', 'id'=>'caption', 'placeholder'=>'Caption']) !!}
                  </div>
                  <div class="caboodle-form-group">
                    <label for="name">Alt</label>
                    {!! Form::text('alt', null, ['class'=>'form-control', 'id'=>'alt', 'placeholder'=>'Alt']) !!}
                  </div>
                  <div class="caboodle-form-group">
                    <label for="name">Tags</label>
                    <select name="tags" class="select2 sumo-asset-tagger"
                    multiple
                    data-url="{{route('adminGetAssetTags')}}"
                    ></select>
                  </div>
                  <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-dismiss="modal">Cancel</button>
        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated btn-select" data-dismiss="modal">Select</button>
      </div>
    </div>
  </div>
</div>
